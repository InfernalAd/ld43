﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStarter : MonoBehaviour {
    private GameController _gameController;

    private void Awake()
    {
        StartGame();
    }

    void StartGame()
    {
        _gameController = new GameController();
        _gameController.Start();
    }

    
}
