﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillagerModel  {
    public string Name;
    public string Profession;

    public int HouseId;
    public int ProblemId;
}
