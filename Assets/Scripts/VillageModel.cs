﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillageModel {
    public int Opinion;
    public Dictionary<DeityId, int> Piety = new Dictionary<DeityId, int>() {
        { DeityId.Deity1,0 },
        { DeityId.Deity2,0 },
        { DeityId.Deity3,0 },
    };


    public List<VillagerModel> Villagers = new List<VillagerModel>();
}
