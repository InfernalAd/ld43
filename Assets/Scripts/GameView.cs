﻿using UnityEngine;
using DG.Tweening;
using System;

public class GameView:MonoBehaviour
{
    [SerializeField] MapScreen _mapScreen;
    [SerializeField] VillageScreen _villageScreen;
    [SerializeField] GameScreen _cityScreen;

    [SerializeField] QuestView _questView;

    GameScreen _currentScreen;
    public Action OnShowVillage;
    public Action OnShowCity;
    public Action OnShowMap;

    public QuestView QuestView => _questView;
    

    private void Awake()
    {
        _mapScreen.VillageBtn.onClick.AddListener(()=> { OnShowVillage?.Invoke(); });
        _mapScreen.CityBtn.onClick.AddListener(() => { OnShowCity?.Invoke(); });

        _villageScreen.ShowQuest = ShowQuest;
    }

    public void ShowMap()
    {
        _currentScreen?.Hide();
        _mapScreen.Show();
        _currentScreen = _mapScreen;
    }

    public void ShowVillage()
    {
        _currentScreen?.Hide();
        _villageScreen.Show();
        _currentScreen = _villageScreen;
    }

    public void ShowCity()
    {
        _currentScreen?.Hide();
        _cityScreen.Show();
        _currentScreen = _cityScreen;
    }

    public void ShowQuest(Quest quest)
    {
        _questView.Show(quest);
    }
}