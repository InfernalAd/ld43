﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ResolutionView : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler {
    [SerializeField] Text _text;
    [SerializeField] GameObject _description;
    [SerializeField] Text _descriptionText;

    public void Init(Resolution resolution)
    {
        _text.text = resolution.Text;
        _descriptionText.text = resolution.Description;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _description.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _description.SetActive(false);
    }
}
