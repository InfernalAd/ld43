﻿using System;
using UnityEngine;

public class GameController
{
    private GameModel _model;
    private GameView _view;

    private ResolutionGenerator _resolutionGenerator = new ResolutionGenerator();

    public GameController()
    {

    }

    public void Start()
    {
        _model = new GameModel();
        _view = GameObject.FindObjectOfType<GameView>();
        _view.OnShowVillage = ShowVillage;
        _view.QuestView.Init(_resolutionGenerator);
        SnowMap();
    }

    private void SnowMap()
    {
        _view.ShowMap();
    }

    private void ShowVillage()
    {
        _view.ShowVillage();
    }

    private void ShowCity()
    {
        _view.ShowCity();
    }

    void MakeMapTurn()
    {
    }

    public void ShowDecision()
    {
    }
}