﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestView : GameScreen {
    [SerializeField] private Text _descriptionText;
    [SerializeField] private GameObject _resolutionContent;
    [SerializeField] private ResolutionView _resolutionPrefab;

    private ResolutionGenerator _resolutionGenerator;

    public void Init(ResolutionGenerator gen)
    {
        _resolutionGenerator = gen;
    }

    public void Show (Quest quest) {
        base.Show();
        _descriptionText.text = quest.Description;

        foreach (var resolutionKey in quest.Resolutions)
        {
            var resolution = _resolutionGenerator.Generate(resolutionKey);
            var rv = Instantiate(_resolutionPrefab, _resolutionContent.transform);
            rv.Init(resolution);
        }
	}
}
