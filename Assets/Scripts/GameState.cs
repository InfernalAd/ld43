﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModel : MonoBehaviour {
    public PlayerModel Player;
    public VillageModel Village;
    public VillageModel City;
}
