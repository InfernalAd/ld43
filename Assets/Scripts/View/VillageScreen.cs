﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillageScreen : GameScreen {

    public Action<Quest> ShowQuest;

    public override void Show()
    {
        base.Show();


        var stubQuest = new Quest()
        {
            Description = "quest_desc",
            Key = "quest_test",
            Resolutions = new List<string>() { "resolution_1", "resolution_2", "resolution_3" }
        };
        ShowQuest?.Invoke(stubQuest);
    }
}
