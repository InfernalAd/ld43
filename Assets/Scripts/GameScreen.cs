﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(CanvasGroup))]
public class GameScreen : MonoBehaviour {
    private CanvasGroup _canvasGroup;

    private void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    public virtual void Hide()
    {
        _canvasGroup.blocksRaycasts = false;
        _canvasGroup.DOFade(0f,1f);
    }

    public virtual void Show()
    {
        _canvasGroup.DOFade(1f, 1f).OnComplete(()=>
        {
            _canvasGroup.blocksRaycasts = true;
        });
    }
}
