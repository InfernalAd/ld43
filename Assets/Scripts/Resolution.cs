﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Resolution  {
    public string Text;
    public string Description;

    public abstract void Activate();
}

public class AddResourceResolution : Resolution
{
    public override void Activate()
    {
        Debug.Log("Add gold");
    }
}
